import { Component, OnInit } from '@angular/core';
import { Artifact } from '../artifact';
import { ArtifactServiceService } from '../artifact-service.service';

@Component({
  selector: 'app-list-artifacts',
  templateUrl: './list-artifacts.component.html',
  styleUrls: ['./list-artifacts.component.css']
})
export class ListArtifactsComponent implements OnInit {

  listArtifacts: Artifact[];

  constructor(private artifactService: ArtifactServiceService) { 
  }

  ngOnInit() {
    this.artifactService.findAll().subscribe(data => {
      this.listArtifacts = data;
    });
    console.log(this.listArtifacts);
  }

  onSubmit() {
    console.log("coucou");
    this.artifactService.exportAllDatabase().subscribe();
  }

}

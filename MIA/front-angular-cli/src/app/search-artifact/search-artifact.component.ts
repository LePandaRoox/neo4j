import { Component, OnInit } from '@angular/core';
import { Artifact } from '../artifact';
import { ArtifactServiceService } from '../artifact-service.service';

@Component({
  selector: 'app-search-artifact',
  templateUrl: './search-artifact.component.html',
  styleUrls: ['./search-artifact.component.css']
})
export class SearchArtifactComponent implements OnInit {

  listArtifacts: Artifact[];
  public artifact: Artifact;

  constructor(private artifactService: ArtifactServiceService) {
    this.artifact = new Artifact();
   }

  ngOnInit() {
  }

  onSubmit() {
    this.artifactService.searchArtifactId(this.artifact).subscribe(data => {
      this.listArtifacts = data;
    });
  }

  onSubmit2() {
    this.artifactService.searchGroupIdArtifact(this.artifact).subscribe(data => {
      this.listArtifacts = data;
    });
  }

  onSubmit3() {
    this.artifactService.searchTitle(this.artifact).subscribe(data => {
      this.listArtifacts = data;
    });
  }

  onSubmit4() {
    this.artifactService.searchVersion(this.artifact).subscribe(data => {
      this.listArtifacts = data;
    });
  }



}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchArtifactComponent } from './search-artifact.component';

describe('SearchArtifactComponent', () => {
  let component: SearchArtifactComponent;
  let fixture: ComponentFixture<SearchArtifactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchArtifactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchArtifactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

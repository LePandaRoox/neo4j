import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingArtifactComponent } from './mapping-artifact.component';

describe('MappingArtifactComponent', () => {
  let component: MappingArtifactComponent;
  let fixture: ComponentFixture<MappingArtifactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MappingArtifactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingArtifactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Artifact } from '../artifact';
import { ArtifactServiceService } from '../artifact-service.service';
import { Plugin } from '../plugin';
import { PluginServiceService } from '../plugin-service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-mapping-artifact',
  templateUrl: './mapping-artifact.component.html',
  styleUrls: ['./mapping-artifact.component.css']
})
export class MappingArtifactComponent implements OnInit {

  artifact: Artifact;
  artifactmapped: Artifact;
  plugin: Plugin;
  pluginmapped: Plugin;
  listArtifacts: Artifact[];
  listPlugins: Plugin[];
  splitter: string[];

  constructor(private route: ActivatedRoute, private router: Router, private artifactService: ArtifactServiceService, private pluginService: PluginServiceService) {
    this.artifact = new Artifact();
    this.artifactmapped = new Artifact();
    this.plugin = new Plugin();
    this.pluginmapped = new Plugin();
    this.splitter = [];
  }

  ngOnInit() {
    this.artifactService.findAll().subscribe(data => {
      this.listArtifacts = data;
    });
    this.pluginService.findAll().subscribe(data => {
      this.listPlugins = data;
    });
  }

  onSubmitArt() {
    console.log(this.artifact, this.artifactmapped);
    this.artifactService.map(this.artifact, this.artifactmapped).subscribe(result => this.gotoArtifactList());
  }


  onSubmitPlug() {
    console.log(this.plugin, this.pluginmapped);
    this.splitter = this.plugin.title.split(" ", 3);
    this.plugin.title = this.splitter[0];
    this.plugin.others_prefix = this.splitter[1];
    this.splitter = this.pluginmapped.title.split(" ", 3);
    this.pluginmapped.title = this.splitter[0];
    this.pluginmapped.others_prefix = this.splitter[1];
    //console.log(this.plugin, this.pluginmapped);
    this.pluginService.map(this.plugin, this.pluginmapped).subscribe(result => this.gotoArtifactList());
  }

  gotoArtifactList() {
    this.router.navigate(['/artifacts/mapping']);
  }
}

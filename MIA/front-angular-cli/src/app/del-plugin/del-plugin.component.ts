import { Component, OnInit } from '@angular/core';
import { Plugin } from '../plugin';
import { PluginServiceService } from '../plugin-service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-del-plugin',
  templateUrl: './del-plugin.component.html',
  styleUrls: ['./del-plugin.component.css']
})
export class DelPluginComponent implements OnInit {

  plugin: Plugin;

  constructor(private route: ActivatedRoute, private router: Router, private pluginService: PluginServiceService) { 
    this.plugin = new Plugin();
  }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.plugin);
    this.pluginService.delete(this.plugin).subscribe(result => this.gotoPluginList());
  }

  gotoPluginList() {
    this.router.navigate(['/plugins']);
  }
}

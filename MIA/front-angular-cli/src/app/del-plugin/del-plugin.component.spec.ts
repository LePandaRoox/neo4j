import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelPluginComponent } from './del-plugin.component';

describe('DelPluginComponent', () => {
  let component: DelPluginComponent;
  let fixture: ComponentFixture<DelPluginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelPluginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelPluginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

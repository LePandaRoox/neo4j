import { Component, OnInit } from '@angular/core';
import { Plugin } from '../plugin';
import { PluginServiceService } from '../plugin-service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-plugins',
  templateUrl: './plugins.component.html',
  styleUrls: ['./plugins.component.css']
})
export class PluginsComponent implements OnInit {

  plugin: Plugin;

  constructor(private route: ActivatedRoute, private router: Router, private pluginService: PluginServiceService) { 
    this.plugin = new Plugin();
  }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.plugin);
    this.pluginService.save(this.plugin).subscribe(result => this.gotoPluginList());
  }

  gotoPluginList() {
    this.router.navigate(['/plugins']);
  }

}

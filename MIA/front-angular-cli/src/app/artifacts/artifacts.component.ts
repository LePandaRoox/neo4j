import { Component, OnInit } from '@angular/core';
import { Artifact } from '../artifact';
import { ArtifactServiceService } from '../artifact-service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-artifacts',
  templateUrl: './artifacts.component.html',
  styleUrls: ['./artifacts.component.css']
})
export class ArtifactsComponent implements OnInit {

  artifact: Artifact;

  constructor(private route: ActivatedRoute, private router: Router, private artifactService: ArtifactServiceService) {
    this.artifact = new Artifact();
  }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.artifact);
    this.artifactService.save(this.artifact).subscribe(result => this.gotoArtifactList());
  }

  gotoArtifactList() {
    this.router.navigate(['/artifacts']);
  }
}

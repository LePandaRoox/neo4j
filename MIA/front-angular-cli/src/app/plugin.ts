export class Plugin {
    id_Plugin : number;
    root : string;
    organisation : string;
    title : string;
    others_prefix : string;
    version : string;
}

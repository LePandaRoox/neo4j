import { Component, OnInit } from '@angular/core';
import { Plugin } from '../plugin';
import { PluginServiceService } from '../plugin-service.service';

@Component({
  selector: 'app-search-plugin',
  templateUrl: './search-plugin.component.html',
  styleUrls: ['./search-plugin.component.css']
})
export class SearchPluginComponent implements OnInit {

  listPlugins: Plugin[];
  public plugin: Plugin;

  constructor(private pluginService: PluginServiceService) {
    this.plugin = new Plugin();
   }

  ngOnInit() {
  }

  onSubmit() {
    this.pluginService.searchPluginId(this.plugin).subscribe(data => {
      this.listPlugins = data;
    });
  }

  onSubmit2() {
    this.pluginService.searchPluginRoot(this.plugin).subscribe(data => {
      this.listPlugins = data;
    });
  }

  onSubmit3() {
    this.pluginService.searchPluginOrganisation(this.plugin).subscribe(data => {
      this.listPlugins = data;
    });
  }

  onSubmit4() {
    this.pluginService.searchPluginTitle(this.plugin).subscribe(data => {
      this.listPlugins = data;
    });
  }

  onSubmit5() {
    this.pluginService.searchPluginOtherPrefix(this.plugin).subscribe(data => {
      this.listPlugins = data;
    });
  }

  onSubmit6() {
    this.pluginService.searchPluginVersion(this.plugin).subscribe(data => {
      this.listPlugins = data;
    });
  }

}

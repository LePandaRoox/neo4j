import { TestBed } from '@angular/core/testing';

import { ArtifactServiceService } from './artifact-service.service';

describe('ArtifactServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArtifactServiceService = TestBed.get(ArtifactServiceService);
    expect(service).toBeTruthy();
  });
});

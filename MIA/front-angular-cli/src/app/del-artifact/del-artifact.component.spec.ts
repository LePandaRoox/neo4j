import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelArtifactComponent } from './del-artifact.component';

describe('DelArtifactComponent', () => {
  let component: DelArtifactComponent;
  let fixture: ComponentFixture<DelArtifactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelArtifactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelArtifactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

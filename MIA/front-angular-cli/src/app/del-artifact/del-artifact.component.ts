import { Component, OnInit } from '@angular/core';
import { Artifact } from '../artifact';
import { ActivatedRoute, Router } from '@angular/router';
import { ArtifactServiceService } from '../artifact-service.service';

@Component({
  selector: 'app-del-artifact',
  templateUrl: './del-artifact.component.html',
  styleUrls: ['./del-artifact.component.css']
})
export class DelArtifactComponent implements OnInit {

  artifact: Artifact;

  constructor(private route: ActivatedRoute, private router: Router, private artifactService: ArtifactServiceService) {
    this.artifact = new Artifact();
  }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.artifact);
    this.artifactService.delete(this.artifact).subscribe(result => this.gotoArtifactList());
  }

  gotoArtifactList() {
    this.router.navigate(['/artifacts']);
  }

}

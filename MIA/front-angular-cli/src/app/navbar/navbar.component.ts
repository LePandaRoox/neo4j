import { Component, OnInit, Input } from '@angular/core';
import { Artifact } from '../artifact';
import { ArtifactServiceService } from '../artifact-service.service';
import { FormBuilder, NgForm, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  listArtifacts: Artifact[];

  public artifact: Artifact;
  
  constructor(private artifactService: ArtifactServiceService) { 
    this.artifact = new Artifact();
  }

  ngOnInit() {
  }

  onSubmit() {
   /* this.artifactService.searchGroupArtifact(this.artifact).subscribe(data => {
      this.listArtifacts = data;
    });
    console.log(this.artifact.title);*/
  }

 

}

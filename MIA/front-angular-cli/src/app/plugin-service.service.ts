import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Plugin } from '../app/plugin';
import { Observable } from 'rxjs/Observable';

const headers = new HttpHeaders()
            .set("Access-Control-Allow-Origin", "*");

@Injectable({
  providedIn: 'root'
})
export class PluginServiceService {

  private pluginUrl: string;

  constructor(private http: HttpClient) {
    this.pluginUrl = 'http://localhost:8080/plugins';
  }

  public findAll(): Observable<Plugin[]> {
    return this.http.get<Plugin[]>(this.pluginUrl + "/list");
  }

  public searchPluginId(plugin: Plugin): Observable<Plugin[]> {
    let parametres = new HttpParams();
    parametres = parametres.append('id_Plugin', plugin.id_Plugin.toString());
    parametres = parametres.append('Access-Control-Allow-Origin', '*');
    return this.http.get<Plugin[]>(this.pluginUrl + "/pluginId", { params: parametres });
  }

  public searchPluginRoot(plugin: Plugin): Observable<Plugin[]> {
    let parametres = new HttpParams();
    parametres = parametres.append('root', plugin.root);
    parametres = parametres.append('Access-Control-Allow-Origin', '*');
    return this.http.get<Plugin[]>(this.pluginUrl + "/root", { params: parametres });
  }

  public searchPluginOrganisation(plugin: Plugin): Observable<Plugin[]> {
    let parametres = new HttpParams();
    parametres = parametres.append('organisation', plugin.organisation);
    parametres = parametres.append('Access-Control-Allow-Origin', '*');
    return this.http.get<Plugin[]>(this.pluginUrl + "/organisation", { params: parametres });
  }

  public searchPluginTitle(plugin: Plugin): Observable<Plugin[]> {
    let parametres = new HttpParams();
    parametres = parametres.append('title', plugin.title);
    parametres = parametres.append('Access-Control-Allow-Origin', '*');
    return this.http.get<Plugin[]>(this.pluginUrl + "/title", { params: parametres });
  }

  public searchPluginOtherPrefix(plugin: Plugin): Observable<Plugin[]> {
    let parametres = new HttpParams();
    parametres = parametres.append('others_prefix', plugin.others_prefix);
    parametres = parametres.append('Access-Control-Allow-Origin', '*');
    return this.http.get<Plugin[]>(this.pluginUrl + "/otherPrefix", { params: parametres });
  }

  public searchPluginVersion(plugin: Plugin): Observable<Plugin[]> {
    let parametres = new HttpParams();
    parametres = parametres.append('version', plugin.version);
    parametres = parametres.append('Access-Control-Allow-Origin', '*');
    return this.http.get<Plugin[]>(this.pluginUrl + "/version", { params: parametres });
  }

  public save(plugin: Plugin) {
    console.log(plugin);
    return this.http.post<Plugin>(this.pluginUrl + "/addPlugin", plugin);
  }

  public delete(plugin: Plugin) {
    return this.http.post<Plugin>(this.pluginUrl + "/delPlugin", plugin);
  }

  public map(plugin: Plugin, pluginmapped: Plugin) {
    return this.http.post<Plugin>(this.pluginUrl + "/mapPlugin", {plugin, pluginmapped});
  }

  public exportAllDatabase() {
    return this.http.get("http://localhost:8080/plugins/export");
  }
}

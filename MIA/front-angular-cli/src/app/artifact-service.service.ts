import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Artifact } from '../app/artifact';
import { Observable } from 'rxjs/Observable';

const headers = new HttpHeaders()
            .set("Access-Control-Allow-Origin", "*");

@Injectable({
  providedIn: 'root'
})
export class ArtifactServiceService {

  private artifactUrl: string;

  constructor(private http: HttpClient) {
    this.artifactUrl = 'http://localhost:8080/artifacts';
  } 

  public findAll(): Observable<Artifact[]> {
    return this.http.get<Artifact[]>(this.artifactUrl + "/list");
  }

  public searchArtifactId(artifact: Artifact): Observable<Artifact[]> {
    let parametres = new HttpParams();
    parametres = parametres.append('id_Artifact', artifact.id_Artifact.toString());
    parametres = parametres.append('Access-Control-Allow-Origin', '*');
    return this.http.get<Artifact[]>(this.artifactUrl + "/artifactId", { params: parametres });
  }

  public searchGroupIdArtifact(artifact: Artifact): Observable<Artifact[]> {
    let parametres = new HttpParams();
    parametres = parametres.append('groupId', artifact.groupId);
    parametres = parametres.append('Access-Control-Allow-Origin', '*');
    return this.http.get<Artifact[]>(this.artifactUrl + "/groupId", { params: parametres });
  }

  public searchTitle(artifact: Artifact): Observable<Artifact[]> {
    let parametres = new HttpParams();
    parametres = parametres.append('title', artifact.title);
    return this.http.get<Artifact[]>(this.artifactUrl + "/title", { params: parametres });
  }

  public searchVersion(artifact: Artifact): Observable<Artifact[]> {
    let parametres = new HttpParams();
    parametres = parametres.append('version', artifact.version);
    return this.http.get<Artifact[]>(this.artifactUrl + "/version", { params: parametres });
  }

  public save(artifact: Artifact) {
    console.log(artifact);
    return this.http.post<Artifact>(this.artifactUrl + "/addArtifact", artifact);
  }

  public delete(artifact: Artifact) {
    return this.http.post<Artifact>(this.artifactUrl + "/delArtifact", artifact);
  }

  public map(artifact: Artifact, artifactmapped: Artifact) {
    return this.http.post<Artifact>(this.artifactUrl + "/mapArtifact", {artifact, artifactmapped});
  }

  public exportAllDatabase() {
    console.log("cocuouc");
    return this.http.get("http://localhost:8080/plugins/export");
  }
}

export class Artifact {
    id_Artifact : number;
    groupId : string;
    title : string;
    version : string;
}

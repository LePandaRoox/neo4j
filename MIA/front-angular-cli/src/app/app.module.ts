import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeBannerComponent } from './home-banner/home-banner.component';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { ArtifactsComponent } from './artifacts/artifacts.component';
import { PluginsComponent } from './plugins/plugins.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BannerComponent } from './banner/banner.component';
import { ListArtifactsComponent } from './list-artifacts/list-artifacts.component';
import { HttpClientModule } from '@angular/common/http';
import { ArtifactServiceService } from './artifact-service.service';
import { PluginServiceService } from './plugin-service.service';
import { ListPluginsComponent } from './list-plugins/list-plugins.component';
import { FooterComponent } from './footer/footer.component';
import { DelArtifactComponent } from './del-artifact/del-artifact.component';
import { DelPluginComponent } from './del-plugin/del-plugin.component';
import { SearchArtifactComponent } from './search-artifact/search-artifact.component';
import { SearchComponent } from './search/search.component';
import { SearchPluginComponent } from './search-plugin/search-plugin.component';
import { MappingArtifactComponent } from './mapping-artifact/mapping-artifact.component';
import { DataTestComponent } from './data-test/data-test.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { AccueilComponent } from './accueil/accueil.component';


const appRoutes: Routes = [
  { path: 'plugins', component: ListPluginsComponent},
  { path: 'artifacts', component: ListArtifactsComponent},
  { path: 'plugins/add', component: PluginsComponent},
  { path: 'plugins/delete', component: DelPluginComponent},
  { path: 'artifacts/add', component: ArtifactsComponent},
  { path: 'artifacts/delete', component: DelArtifactComponent},
  { path: 'artifacts/mapping', component: MappingArtifactComponent},
  { path: 'search', component: SearchComponent},
  { path: 'search/artifacts', component: SearchArtifactComponent},
  { path: 'search/plugins', component: SearchPluginComponent},
  { path: '', component: AccueilComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeBannerComponent,
    HomeComponent,
    ArtifactsComponent,
    PluginsComponent,
    NavbarComponent,
    BannerComponent,
    ListArtifactsComponent,
    ListPluginsComponent,
    FooterComponent,
    DelArtifactComponent,
    DelPluginComponent,
    SearchArtifactComponent,
    SearchComponent,
    SearchPluginComponent,
    MappingArtifactComponent,
    DataTestComponent,
    AccueilComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    ReactiveFormsModule
  ],
  providers: [ArtifactServiceService, PluginServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }

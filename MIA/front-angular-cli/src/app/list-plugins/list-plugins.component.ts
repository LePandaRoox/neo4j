import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Plugin } from '../plugin';
import { PluginServiceService } from '../plugin-service.service';

@Component({
  selector: 'app-list-plugins',
  templateUrl: './list-plugins.component.html',
  styleUrls: ['./list-plugins.component.css']
})
export class ListPluginsComponent implements OnInit {

  listPlugins: Plugin[];

  constructor(private pluginService: PluginServiceService) { }

  ngOnInit() {
    this.pluginService.findAll().subscribe(data => {
      this.listPlugins = data;
    });
  }

  onSubmit() {
    this.pluginService.exportAllDatabase().subscribe();
  }

}

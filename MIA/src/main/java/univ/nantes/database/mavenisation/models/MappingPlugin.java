package univ.nantes.database.mavenisation.models;

public class MappingPlugin {
	Plugin plugin;
	Plugin pluginmapped;
	public Plugin getPlugin() {
		return plugin;
	}
	public void setPlugin(Plugin plugin) {
		this.plugin = plugin;
	}
	public Plugin getPluginmapped() {
		return pluginmapped;
	}
	public void setPluginmapped(Plugin pluginmapped) {
		this.pluginmapped = pluginmapped;
	}
}

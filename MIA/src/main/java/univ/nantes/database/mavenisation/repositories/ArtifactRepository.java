package univ.nantes.database.mavenisation.repositories;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import univ.nantes.database.mavenisation.models.Artifact;

@Repository
public interface ArtifactRepository extends Neo4jRepository<Artifact, Long> {
	Artifact findByTitle(@Param("title") String title);

	Iterable<Artifact> findAll();

	@Query("MATCH (p:Artifact {id_Artifact: {0}}) " + "RETURN p")
	Iterable<Artifact> findByArtifactId(int pId);
	
	@Query("MATCH (p:Artifact {groupId: {0}}) " + "RETURN p")
	Iterable<Artifact> findByArtifactGroupId(String pGroupId);
	
	@Query("MATCH (p:Artifact {title: {0}}) " + "RETURN p")
	Iterable<Artifact> findByArtifactTitle(String pTitle);
	
	@Query("MATCH (p:Artifact {version: {0}}) " + "RETURN p")
	Iterable<Artifact> findByArtifactVersion(String pVersion);

	@Query("MERGE (id:UniqueId{name:'Artifact'}) " + "ON CREATE SET id.count = 1 "
			+ "ON MATCH SET id.count = id.count + 1 " + "WITH id.count AS uid "
			+ "CREATE (artifact:Artifact { id_Artifact: uid, groupId: {0}, title: {1}, version: {2} })")
	void createNode(String groupId, String title, String version);

	@Query("MATCH (n { id_Artifact: {0}}) " + "DETACH DELETE n")
	void deleteNode(int pId);
	
	@Query("MATCH (a:Artifact),(b:Artifact) " + 
			"WHERE a.title = {0} AND b.title = {1} " + 
			"CREATE (a)-[r:CompileDependsOn]->(b) " + 
			"RETURN r")
	void addMappingArtifact(String pArtifact1, String pArtifact2);
}

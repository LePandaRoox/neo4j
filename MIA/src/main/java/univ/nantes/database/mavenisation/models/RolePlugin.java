package univ.nantes.database.mavenisation.models;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = "ASSOCIATE_PLUGIN_IN")
public class RolePlugin {
   @Id @GeneratedValue private Long id;
   private List<String> roles = new ArrayList<>();
   @StartNode
   private Plugin pluginStart;
   @EndNode
   private Plugin pluginEnd;
   
   public List<String> getRoles() {
	   return roles;
   }
   public void setRoles(List<String> roles) {
	   this.roles = roles;
   }
}
package univ.nantes.database.mavenisation.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import univ.nantes.database.mavenisation.models.Artifact;
import univ.nantes.database.mavenisation.models.MappingArtifact;
import univ.nantes.database.mavenisation.repositories.ArtifactRepository;

@Service
public class ArtifactService {
	private final static Logger LOG = LoggerFactory.getLogger(ArtifactService.class);
	private final ArtifactRepository artifactRepository;

	public ArtifactService(ArtifactRepository artifactrepository) {
		this.artifactRepository = artifactrepository;
	}

	@Transactional(readOnly = true)
	public List<Artifact> findByArtifactId(int pArtifactId) {
		List<Artifact> artifacts = new ArrayList<Artifact>();
		for (Artifact iter : artifactRepository.findByArtifactId(pArtifactId)) {
			artifacts.add(iter);
		}
		return artifacts;
	}
	
	@Transactional(readOnly = true)
	public List<Artifact> findByArtifactGroupId(String pArtifactGroupId) {
		List<Artifact> artifacts = new ArrayList<Artifact>();
		for (Artifact iter : artifactRepository.findByArtifactGroupId(pArtifactGroupId)) {
			artifacts.add(iter);
		}
		return artifacts;
	}
	
	@Transactional(readOnly = true)
	public List<Artifact> findByArtifactTitle(String artifact_name) {
		List<Artifact> artifacts = new ArrayList<Artifact>();
		for (Artifact iter : artifactRepository.findByArtifactTitle(artifact_name)) {
			artifacts.add(iter);
		}
		return artifacts;
	}

	@Transactional(readOnly = true)
	public List<Artifact> findByArtifactVersion(String pArtifactVersion) {
		List<Artifact> artifacts = new ArrayList<Artifact>();
		for (Artifact iter : artifactRepository.findByArtifactVersion(pArtifactVersion)) {
			artifacts.add(iter);
		}
		return artifacts;
	}
	
	@Transactional(readOnly = true)
	public List<Artifact> listAll() {
		List<Artifact> artifacts = new ArrayList<Artifact>();
		for (Artifact iter : artifactRepository.findAll()) {
			artifacts.add(iter);
		}
		return artifacts;
	}

	@Transactional
	public void createArtifacts(@RequestBody Artifact artifact) {
		artifactRepository.createNode(artifact.getGroupId(), artifact.getTitle(), artifact.getVersion());
	}

	@Transactional
	public void mapArtifact(@RequestBody MappingArtifact artifact) {
		artifactRepository.addMappingArtifact(artifact.getArtifact().getTitle(), artifact.getArtifactmapped().getTitle());
	}
	
	@Transactional
	public void deleteArtifacts(@RequestBody Artifact artifact) {
		artifactRepository.deleteNode((int) artifact.getId_Artifact());
	}

}

package univ.nantes.database.mavenisation.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import univ.nantes.database.mavenisation.models.Artifact;
import univ.nantes.database.mavenisation.models.MappingPlugin;
import univ.nantes.database.mavenisation.models.Plugin;
import univ.nantes.database.mavenisation.repositories.PluginRepository;

@Service
public class PluginService {
	private final static Logger LOG = LoggerFactory.getLogger(PluginService.class);

	public final PluginRepository pluginRepository;

	public PluginService(PluginRepository pPluginRepository) {
		this.pluginRepository = pPluginRepository;
	}

	@Transactional(readOnly = true)
	public List<Plugin> findByPluginId(int pPluginId) {
		List<Plugin> plugins = new ArrayList<Plugin>();
		for (Plugin iter : pluginRepository.findByPluginId(pPluginId)) {
			plugins.add(iter);
		}
		return plugins;
	}
	
	@Transactional(readOnly = true)
	public List<Plugin> findByPluginRoot(String pPluginRoot) {
		List<Plugin> plugins = new ArrayList<Plugin>();
		for (Plugin iter : pluginRepository.findByPluginRoot(pPluginRoot)) {
			plugins.add(iter);
		}
		return plugins;
	}
	
	@Transactional(readOnly = true)
	public List<Plugin> findByPluginOrganisation(String pPluginOrganisation) {
		List<Plugin> plugins = new ArrayList<Plugin>();
		for (Plugin iter : pluginRepository.findByPluginOrganisation(pPluginOrganisation)) {
			plugins.add(iter);
		}
		return plugins;
	}
	
	@Transactional(readOnly = true)
	public List<Plugin> findByPluginTitle(String pPluginTitle) {
		List<Plugin> plugins = new ArrayList<Plugin>();
		for (Plugin iter : pluginRepository.findByPluginTitle(pPluginTitle)) {
			plugins.add(iter);
		}
		return plugins;
	}
	
	@Transactional(readOnly = true)
	public List<Plugin> findByPluginOtherPrefix(String pPluginOtherPrefix) {
		List<Plugin> plugins = new ArrayList<Plugin>();
		for (Plugin iter : pluginRepository.findByPluginOthersPrefix(pPluginOtherPrefix)) {
			plugins.add(iter);
		}
		return plugins;
	}
	
	@Transactional(readOnly = true)
	public List<Plugin> findByPluginVersion(String pPluginVersion) {
		List<Plugin> plugins = new ArrayList<Plugin>();
		for (Plugin iter : pluginRepository.findByPluginVersion(pPluginVersion)) {
			plugins.add(iter);
		}
		return plugins;
	}

	@Transactional(readOnly = true)
	public List<Plugin> listAll() {
		List<Plugin> plugins = new ArrayList<Plugin>();
		for (Plugin iter : pluginRepository.findAll()) {
			plugins.add(iter);
		}
		return plugins;
	}

	@Transactional
	public void createPlugin(@RequestBody Plugin plugin) {
		pluginRepository.createNode(plugin.getRoot(), plugin.getOrganisation(), plugin.getTitle(),
				plugin.getOthers_prefix(), plugin.getVersion());
	}

	@Transactional
	public void mapPlugin(@RequestBody MappingPlugin plugin) {
		pluginRepository.addMappingPlugin(plugin.getPlugin().getTitle(), plugin.getPluginmapped().getTitle(), plugin.getPlugin().getOthers_prefix(), plugin.getPluginmapped().getOthers_prefix());
	}
	
	@Transactional
	public void deletePlugin(@RequestBody Plugin plugin) {
		pluginRepository.deleteNode((int) plugin.getId_Plugin());
	}
	
	@Transactional
	public void exportDatabase() {
		pluginRepository.exportAllDatabase();
	}
}

package univ.nantes.database.mavenisation.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.neo4j.driver.internal.shaded.reactor.util.annotation.NonNull;
import org.neo4j.driver.internal.shaded.reactor.util.annotation.Nullable;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@NodeEntity
public class Artifact {
	@Id
	@Index
	private long id_Artifact;

	private String groupId;
	private String title;
	private String version;

	@Relationship(type = "ASSOCIATE_ARTIFACT_IN")
	private List<Artifact> artifacts = new ArrayList<Artifact>();

	@Nullable
	@JsonIgnoreProperties("artifact")
	@Relationship(type = "ASSOCIATE_ARTIFACT_IN", direction = Relationship.INCOMING)
	private List<RoleArtifact> roles;

	public Artifact() {
	}
	
	public Artifact(long pId_Artifact, String pGroupId, String pTitle, String pVersion) {
		this.id_Artifact = pId_Artifact;
		this.groupId = pGroupId;
		this.title = pTitle;
		this.version = pVersion;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getTitle() {
		return title;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void addRole(Artifact role) {
		if (this.artifacts == null) {
			this.artifacts = new ArrayList<>();
		}
		this.artifacts.add(role);
	}
	
	public long getId_Artifact() {
		return id_Artifact;
	}

	public void setId_Artifact(long id_Artifact) {
		this.id_Artifact = id_Artifact;
	}
}

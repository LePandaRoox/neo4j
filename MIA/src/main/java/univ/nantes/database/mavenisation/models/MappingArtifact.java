package univ.nantes.database.mavenisation.models;

public class MappingArtifact {
	Artifact artifact;
	Artifact artifactmapped;
	
	public Artifact getArtifact() {
		return artifact;
	}
	public void setArtifact(Artifact artifact) {
		this.artifact = artifact;
	}
	public Artifact getArtifactmapped() {
		return artifactmapped;
	}
	public void setArtifactmapped(Artifact artifactmapped) {
		this.artifactmapped = artifactmapped;
	}
}

package univ.nantes.database.mavenisation.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@NodeEntity
@Table(name = "P01_Plugins")
public class Plugin {
	@Id
	@GeneratedValue
	private long id_Plugin;

	private String root;
	private String organisation;
	private String title;
	private String others_prefix;
	private String version;

	@Relationship(type = "ASSOCIATE_PLUGIN_IN")
	private List<Plugin> plugins = new ArrayList<Plugin>();

	@JsonIgnoreProperties("plugin")
	@Relationship(type = "ASSOCIATE_PLUGIN_IN", direction = Relationship.INCOMING)
	private List<RolePlugin> roles;

	public Plugin() {
	}

	public long getId_Plugin() {
		return id_Plugin;
	}

	public void setId_Plugin(int id_Plugin) {
		this.id_Plugin = id_Plugin;
	}

	public String getRoot() {
		return root;
	}

	public void setRoot(String root) {
		this.root = root;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getOthers_prefix() {
		return others_prefix;
	}

	public void setOthers_prefix(String others_prefix) {
		this.others_prefix = others_prefix;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}

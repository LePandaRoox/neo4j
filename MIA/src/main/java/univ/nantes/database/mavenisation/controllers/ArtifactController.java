package univ.nantes.database.mavenisation.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import univ.nantes.database.mavenisation.models.Artifact;
import univ.nantes.database.mavenisation.models.MappingArtifact;
import univ.nantes.database.mavenisation.services.ArtifactService;

@RestController
@RequestMapping("/artifacts")
@CrossOrigin(origins = "http://localhost:4200")
public class ArtifactController {
	private final ArtifactService artifactService;

	public ArtifactController(ArtifactService pArtifactService) {
		this.artifactService = pArtifactService;
	}

	@GetMapping("/artifactId")
	public List<Artifact> findByArtifactId(@RequestParam(value = "id_Artifact") String pArtifactId) {
		return artifactService.findByArtifactId(Integer.parseInt(pArtifactId));
	}
	
	@GetMapping("/groupId")
	public List<Artifact> findByArtifactGroupId(@RequestParam(value = "groupId") String pArtifactGroupId) {
		return artifactService.findByArtifactGroupId(pArtifactGroupId);
	}
	
	@GetMapping("/title")
	public List<Artifact> findByArtifactTitle(@RequestParam(value = "title") String pArtifactTitle) {
		return artifactService.findByArtifactTitle(pArtifactTitle);
	}
	
	@GetMapping("/version")
	public List<Artifact> findByArtifactVersion(@RequestParam(value = "version") String pArtifactVersion) {
		return artifactService.findByArtifactVersion(pArtifactVersion);
	}

	@GetMapping("/list")
	public List<Artifact> findAll() {
		return artifactService.listAll();
	}

	@PostMapping("/addArtifact")
	public void addArtifact(@RequestBody Artifact artifact) {
		artifactService.createArtifacts(artifact);
	}

	@PostMapping("/mapArtifact") 
	public void mapArtifact(@RequestBody MappingArtifact artifactmap) {
		artifactService.mapArtifact(artifactmap);
	}
	
	@PostMapping("/delArtifact")
	public void deleteArtifact(@RequestBody Artifact artifact) {
		artifactService.deleteArtifacts(artifact);
	}
}

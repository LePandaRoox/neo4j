package univ.nantes.database.mavenisation.repositories;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import univ.nantes.database.mavenisation.models.Artifact;
import univ.nantes.database.mavenisation.models.Plugin;

@Repository
public interface PluginRepository extends Neo4jRepository<Plugin, Long> {
	Plugin findByTitle(@Param("title") String title);

	Iterable<Plugin> findAll();
	
	@Query("MATCH (p:Plugin {id_Plugin: {0}}) " + "RETURN p")
	Iterable<Plugin> findByPluginId(int pId);
	
	@Query("MATCH (p:Plugin {root: {0}}) " + "RETURN p")
	Iterable<Plugin> findByPluginRoot(String pRoot);
	
	@Query("MATCH (p:Plugin {organisation: {0}}) " + "RETURN p")
	Iterable<Plugin> findByPluginOrganisation(String pOrganisation);
	
	@Query("MATCH (p:Plugin {title: {0}}) " + "RETURN p")
	Iterable<Plugin> findByPluginTitle(String pTitle);
	
	@Query("MATCH (p:Plugin {others_prefix: {0}}) " + "RETURN p")
	Iterable<Plugin> findByPluginOthersPrefix(String pOthers_prefix);
	
	@Query("MATCH (p:Plugin {version: {0}}) " + "RETURN p")
	Iterable<Plugin> findByPluginVersion(String pVersion);

	@Query("MERGE (id:UniqueId{name:'Plugin'}) " + "ON CREATE SET id.count = 1 "
			+ "ON MATCH SET id.count = id.count + 1 " + "WITH id.count AS uid "
			+ "CREATE (plugin:Plugin {id_Plugin: uid, root: {0}, organisation: {1}, title: {2}, others_prefix: {3}, version: {4}})")
	void createNode(String root, String organisation, String title, String others_prefix, String version);

	@Query("MATCH (n { id_Plugin: {0}}) " + "DETACH DELETE n")
	void deleteNode(int pId);
	
	@Query("MATCH (a:Plugin),(b:Plugin) " + 
			"WHERE a.title = {0} AND b.title = {1} AND a.others_prefix = {2} AND b.others_prefix = {3}" + 
			"CREATE (a)-[r:CompileDependsOn]->(b) " + 
			"RETURN r")
	void addMappingPlugin(String pPlugin1Title, String pPlugin2Title, String pPlugin1Other, String pPlugin2Other);
	
	@Query("call apoc.export.json.all('exportGraph.json', {})")
	void exportAllDatabase();	
}

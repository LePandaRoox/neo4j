package univ.nantes.database.mavenisation.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import univ.nantes.database.mavenisation.models.Artifact;
import univ.nantes.database.mavenisation.models.MappingPlugin;
import univ.nantes.database.mavenisation.models.Plugin;
import univ.nantes.database.mavenisation.services.PluginService;

@RestController
@RequestMapping("/plugins")
@CrossOrigin(origins = "http://localhost:4200")
public class PluginController {
	private final PluginService pluginService;

	public PluginController(PluginService pPluginService) {
		this.pluginService = pPluginService;
	}

	@GetMapping("/pluginId")
	public List<Plugin> findByPluginId(@RequestParam(value = "id_Plugin") String pPluginId) {
		return pluginService.findByPluginId(Integer.parseInt(pPluginId));
	}
	
	@GetMapping("/root")
	public List<Plugin> findByPluginRoot(@RequestParam(value = "root") String pPluginRoot) {
		return pluginService.findByPluginRoot(pPluginRoot);
	}

	@GetMapping("/organisation")
	public List<Plugin> findByPluginOrganisation(@RequestParam(value = "organisation") String pPluginOrganisation) {
		return pluginService.findByPluginOrganisation(pPluginOrganisation);
	}
	
	@GetMapping("/title")
	public List<Plugin> findByPluginTitle(@RequestParam(value = "title") String pPluginTitle) {
		return pluginService.findByPluginTitle(pPluginTitle);
	}
	
	@GetMapping("/otherPrefix")
	public List<Plugin> findByPluginOtherPrefix(@RequestParam(value = "others_prefix") String pPluginOtherPrefix) {
		return pluginService.findByPluginOtherPrefix(pPluginOtherPrefix);
	}
	
	@GetMapping("/version")
	public List<Plugin> findByPluginVersion(@RequestParam(value = "version") String pPluginVersion) {
		return pluginService.findByPluginVersion(pPluginVersion);
	}
	
	@GetMapping("/list")
	public List<Plugin> findAll() {
		return pluginService.listAll();
	}

	@PostMapping("/addPlugin")
	public void addPlugin(@RequestBody Plugin plugin) {
		pluginService.createPlugin(plugin);
	}
	
	@PostMapping("/mapPlugin") 
	public void mapPlugin(@RequestBody MappingPlugin plugin) {
		pluginService.mapPlugin(plugin);
	}

	@PostMapping("/delPlugin")
	public void deleteArtifact(@RequestBody Plugin plugin) {
		pluginService.deletePlugin(plugin);
	}
	
	@GetMapping("/export")
	public void exportAllDatabase() {
		pluginService.exportDatabase();
	}
	
	
}

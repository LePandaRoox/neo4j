# MAVENISATION

Mavenisation est une application permettant de communiquer avec une base de données orientée graphe et d'effectuer les actions suivantes sur une interface client :

*  Ajouter des plugins et Artifacts dans la base.
*  Supprimer des plugins et Artifacts dans la base.
*  Rechercher des plugins et Artifacts dans la base.
*  Effectuer un mapping entre plugins ou artifacts.
*  Exporter la'état de la base au format 'json'.

Pour utiliser l'application "Mavenisation", il faut configurer 3 grandes parties composant l'infrastructure du projet.

## Neo4j (BASE DE DONNÉES)

### Installation de Neo4j 

Pour installer la partie base de données de l'application, suivre le tutoriel pour l'installation de Neo4j Desktop sur le site `https://neo4j.com/developer/neo4j-desktop/`.

### Configuration Neo4j 

Ensuite pour configurer, il faut créer un nouveau projet composé de l'application `Neo4j Browser` (normalement présent de base lors de la création d'un projet). Puis il faut ajouter un graphe à l'aide de la section `Add Graph` avec la configuration suivante :

| Nom du graphe | mot de passe |
| ------ | ------ |
| neo4j | secret |

Information : Pour changer le nom du graphe et le mot de passe, voir la section ci-dessous sur `l'infrastructure du manager`. 

Pour finir, il faut ajouter le plugin à l'aide de la section `Add Plugin` et configurer le fichier `setting` :

Pour accèder au fichier : Bouton `Manage` >  Onglet `Settings`.

```
apoc.trigger.enabled=true
apoc.ttl.enabled=true
apoc.import.file.enabled=true
apoc.export.file.enabled=true
apoc.export.file.enabled=true
apoc.import.file.enabled=true
```
Les lignes ci-dessus permettent l'utilisation des différentes fonctions `import/export` avec le langage Cypher. 

La base peux ensuite être lancée avec le bouton `Start`.

## Spring Boot Java (MANAGER)

### Importation du projet

Télécharger le projet à partir de git à l'aide du bouton `Clone` en haut à droite. (utilisation de la commande `git clone [URL projet]` dans un terminal) 

Une fois le projet téléchargé, importer le projet dans eclipse : `File > Open Projects From File System`.

Le projet est organisé de la façon suivante :

```
mavenisation
    |- src/main/java 
    |    |- univ.nantes.database.mavenisation (contient la classe principale `App.java`)
    |        |- univ.nantes.database.mavenisation.controllers (contient les controlleurs)
    |        |- univ.nantes.database.mavenisation.models (contient les modèles)
    |        |- univ.nantes.database.mavenisation.repositories (contient les repositories)
    |        |- univ.nantes.database.mavenisation.services (contient les services)
    |
    |- src/main/resources (contient le fichier `application.properties` pour la configuration avec la base de données Neo4j)
```

Information : Pour changer le nom du graphe et le mot de passe, il faut ouvrir le fichier `application.properties` et modifier les champs `spring.data.neo4j.username=neo4j` et `spring.data.neo4j.password=secret`.

### Lancement du projet

Pour compiler le projet, faite un clique droit sur le fichier `App.java` se situant dans `src/main/java/univ.nantes.database.mavenisation`.

## Angular (CLIENT)

### Installation d'Angular

Pour installer angular, il faut effectuer la commande `npm install -g @angular/cli` dans un terminal.

Une fois angular installé, se rendre dans le dossier `neo4j/MIA/front-angular-cli` du projet cloné précédemment et effectuer les commandes suivantes pour télécharger les dépendances nécessaires :

*  npm install npm@latest -g
*  npm install --save-dev @angular/cli@latest
*  npm install --save @angular/animations
*  npm install --save @angular/material
*  npm install --save rxjs-compat
*  npm i @angular/common
*  npm i @angular/cdk
*  ng update

### Lancement du client

Pour lancer l'interface client, il faut effectuer la commande `ng serve --open` dans le dossier `neo4j/MIA/front-angular-cli`. Cette commande lance un nouvel onglet dans le navigateur à l'adresse : `http://localhost:4200`.

## FAQ

### Pourquoi les données ne s'affiche pas côté client dans l'application ?

Ce problème peux subvenir si aucune connexion n'est présente entre la base Neo4j, Spring et Angular. Pour résoudre le problème, vérifier que la base de données Neo4j est connectée et que la compilation du fichier `App.java` est en cours.

--- 

### Ou se trouve le fichier, une fois l'exportation effectuée ?

Lors de l'appuie sur le bouton `exportation des données`, un fichier `graph.json` est généré dans le dossier `import` du serveur Neo4j. Pour accèder à ce dossier, il faut cliquer sur le bouton `manage` du graphe, puis séléctionner `Open Folder > import` dans l'application `Neo4j Desktop`.
Ensuite un dossier contenant le fichier désiré s'ouvre à l'écran.

